%moja reprezentacja to rep(DrzewoTranzycji, ListaStanow, Alfabet)

%sprawdza czy Aut jest poprawnym automatem, tworzona jest wlasna
%reprezentacja automatu
correct(Aut, Rep) :-
    ground(Aut),
    \+ ground(Rep),
    correctp(Aut, Rep).


correctp(dfa(Funs, Startt, Fin), rep(Tree, States, Alphabet)) :-
    prepro(Startt, Start), %pomocznie, robie singleton
    createBST(Funs, Tree), %tworzy drzewo bst tranzycji 
    createBSTTrans(Funs, TransTree), %tworzy drzewo bst (stan, literka)
    wszerzZnaki(TransTree, Trans), %bfs, wynik to lista [(stan, literka)]
    sameLength(Funs, Trans), %czy nie ma kilkukrotnego wyjscia ze stanu po tej samej literce
    createBSTLet(Funs, AlphTree), %tworzy drzewo bst z samymi literami (bez powtorzen)
    wszerzZnaki(AlphTree, Alphabet), %bfs po drzewie, wynikiem jest lista literek - alfabet
    nonEmptyAlphabet(Alphabet), %sprawdza czy niepusty alfabet
    createBSTStates(Funs, Start, Fin, StatesTree), %drzewo bst z samymi stanami (bez powtorzen) 
    wszerzZnaki(StatesTree, States), %bsf po drzewie stanow, wynik to lista stanow
    checkerAllTrans(Alphabet, States, TransTree).  %sprawdzam czy dla kazdego stanu jest przejscie po kazdej literce


empty(Aut) :-
    correctp(Aut, Rep),
    emptyp(Aut, Rep).

%pomocznie empty, tutaj robi cala robote
%jesli stany koncowe sa puste to sukces, wpp trzeba sprawdzic czy 
%nie istnieja sciezki ze stanu poczatkowego do koncowych
emptyp(dfa(_, Start, Fin), rep(Tree, _, _)) :-
    (Fin == [] -> true 
    ; (createBST(Fin, FinTree),
    checkIfPathNotExist(Start, FinTree, Tree))).


accept(Aut, Slowo) :-
    correctp(Aut, Rep),
    acceptp(Aut, Rep, Slowo).

%pomocniczy accept, sprawdza ile jest stanow, ta liczba pomaga w okresleniu
%czy jezyk danego automatu jest skonczony
%https://cs.stackexchange.com/questions/73993/how-to-determine-if-an-automata
%-dfa-accepts-an-infinite-or-finite-language

acceptp(dfa(_, Startt, Fin), rep(Tree, States, _), Slowo) :-
    createBST(Fin, FinTree),
    list_length(States, Len),
    isFinitAutomata(Startt, FinTree, Tree, Len, 0, Slowo).


%to sprawdzanie zawierania sie w dwie strony
equal(Aut1, Aut2) :-
    subsetEq(Aut1, Aut2),
    subsetEq(Aut2, Aut1).


subsetEq(Aut1, Aut2) :-
    correctp(Aut1, Rep1),
    correctp(Aut2, Rep2),
    subsetEqp(Aut1, Aut2, Rep1, Rep2).

%pomocnicze subset, najpierw sprawdzane sa alfabetym czy sa identyczne,
%potem dla drugiego automatu tworzony jest automat bedacy jego dopelnieniem
%z pierwszego automatu oraz dopelnienia zostaje stworzony automat, ktory 
%rozponaje przecie dwoch jezykow - jest to iloczyn kartezjanski dwoch automatow
%nastepnie sprawdzane jest czy ten automat rozponaje jezyk pusty
%https://cs.stackexchange.com/questions/9130/testing-whether-the-language-of- 
%one-automaton-is-a-subset-of-another

subsetEqp(dfa(_, Startt1, Fin1), Aut2, rep(Tree1, States1, Alphabet1),
    rep(Tree2, States2, Alphabet2)) :-
        isSameList(Alphabet1, Alphabet2), %sprawdza czy te same Alfabety
        complementAutomata(Aut2, dfa(_, Startt2, Fin2)), 
        createBST(Fin1, Fin1Tree),
        createBST(Fin2, Fin2Tree),
        mergeStates(States1, States2, MergedStated), %łączy dwie listy stanów
        createInterTrans(Alphabet1, MergedStated, Tree1, Tree2, NewTrans),
        createInterFin(MergedStated, Fin1Tree, Fin2Tree, NewFin),
        correctp(dfa(NewTrans, ns(Startt1, Startt2), NewFin), Rep),
        emptyp(dfa(NewTrans, ns(Startt1, Startt2), NewFin), Rep).




%tworzy nowy DFA, będący uzupełnieniem poprzedniego (zmiena stanów końcowych)
complementAutomata(dfa(Funs, Startt, Fin), dfa(Funs, Startt, NewFin)) :-
    prepro(Startt, Start),
    createBST(Fin, FinTree),
    createBSTStates(Funs, Start, Fin, StatesTree),
    wszerzZnaki(StatesTree, States),
    getNewFinStates(States, FinTree,  [], NewFin).



getNewFinStates([], _, Acc, Acc).
getNewFinStates([S | SS], FinTree, Acc, NewFin) :-
    (isMemberBST(FinTree, S) -> getNewFinStates(SS, FinTree, Acc, NewFin)
    ; getNewFinStates(SS, FinTree, [S | Acc], NewFin)).
   


%sprawdza automat czy akceptuje skonczone slowa
%pozbywa sie petlenia w nieakceptujacym stanie 

isFinitAutomata(S, FinTree, _, _, _, []) :- 
    isMemberBST(FinTree, S).

isFinitAutomata(S, FinTree, Tree, MaxLen, CurLen, [L | YS]) :-
    isMemberBST(Tree, fp(S, L, Y)),
    (CurLen > MaxLen -> fail
    ;(isMemberBST(FinTree, Y) -> CurLen1 = 0
    ; CurLen1 is CurLen + 1),
    isFinitAutomata(Y, FinTree, Tree, MaxLen, CurLen1, YS)).


%do sprawdzania czy automat ma pusty jezyk
%sprawdza czy ze stanu poczatkowego mozna dojsc do koncowego
%po przejsciu do kolejnego stanu, tranzycja jest usuwana
%zapobiega to petleniu sie
checkIfPathNotExist(_, _, empty).
checkIfPathNotExist(X, FinTree, tree(L, W, P)) :-
    (isMemberBST(tree(L, W, P), fp(X, _, Y)) -> %czy sa jeszcze przejscia
    \+ isMemberBST(FinTree, Y),              %zaprzeczenie czy stan jest koncowy
    removeNodeBst(tree(L, W, P), fp(X, _, Y), ND), %usuwanie tranzycji
    checkIfPathNotExist(Y, FinTree, ND)            
    ; true).



%usuwanie wezla z drzewa BST
removeNodeBst(tree(empty, W, P), W, P) :- !.

removeNodeBst(tree(L, W, empty), W, L) :- !.

removeNodeBst(tree(empty, W, empty), E,  tree(empty, W, empty)) :- E \== W, !.
removeNodeBst(tree(L, W, P), E, tree(L, NE, NP)) :- 
    E == W, !, 
    getLeftMost(P, NE), removeNodeBst(P, NE, NP).

removeNodeBst(tree(L, W, P), E, tree(NLD, W, P)) :-
    E @< W, !, removeNodeBst(L, E, NLD).

removeNodeBst(tree(L, W, P), E, tree(L, W, NPD)) :-
    E @> W, removeNodeBst(P, E, NPD).

%zwraca najwieksza wartosc z lewgo poddrzewa
getLeftMost(tree(empty, W, _), W).
getLeftMost(tree(tree(LL, WL, PL), _, _), NE) :- getLeftMost(tree(LL, WL, PL), NE).


%sprawdza dlugosc listy
list_length(Xs,L) :- list_length(Xs,0,L) .

list_length( []     , L , L ) .
list_length( [_|Xs] , T , L ) :-
  T1 is T+1 ,
  list_length(Xs,T1,L)
  .

% jezeli jest to element to robie singleton, wpp normalnie lista - pomocnicza
prepro(L, Res) :- (isList(L) -> Res = L; Res = [L]).

% sprawdza czy lista
isList([]).
isList([_ | _]).

%sprawdza czy obie listy sa tej samej dlugosci
sameLength([], []).
sameLength([_ | XS], [_ | YS]) :- sameLength(XS, YS).

%sprawdza czy takie same listy - zalozenie to listy sa bez powotrzen
isSameList(L1, L2) :-
    sameLength(L1, L2),
    createBST(L2, Tree),
    allElementsInTree(L1, Tree).

allElementsInTree([], _).
allElementsInTree([X | XS], Tree) :-
    isMemberBST(Tree, X),
    allElementsInTree(XS, Tree).

%drzewo otwarte 
%var(D) -- drzewo jest puste
%wstawianie do drzewa pustego
insertBST(D, E) :- var(D), !, D = tree(_, E, _).
insertBST(tree(_, W, _), E) :- E == W, !, nonvar(W).
insertBST(tree(L, W, _), E) :- E @< W, !, nonvar(W), insertBST(L, E).
insertBST(tree(_, W, R), E) :- E @> W, insertBST(R, E). 


%zamykanie drzewa
closeBST(D) :- var(D), !, D = empty.
closeBST(D) :- nonvar(D), D=tree(L, _, P), closeBST(L), closeBST(P). 


%tworzenie otwartego drzewa bst
%moje drzewa nie trzymaja powtorzen
createBST([], D) :- closeBST(D).
createBST([F | FS], D) :-
    insertBST(D, F),
    createBST(FS, D).

%tworzenie otwartego drzewa bst ale odpowiednie tranzycje jako wierzcholki
createBSTTrans([], D) :- closeBST(D).
createBSTTrans([fp(S1, W, _) | FS], D) :-
    insertBST(D, fpp(S1, W)),
    createBSTTrans(FS, D).

%tworzenie otwartego drzewa bst ale odpowiednie zmienne jako wierzcholki
createBSTLet([], D) :- closeBST(D).
createBSTLet([fp(_, W, _) | FS], D) :-
    insertBST(D, W),
    createBSTLet(FS, D).

%tworzy otwarte bst z 3 list - podaje tutaj tranzycje, stany poczatkowe i koncowe
%utworzone zostanie drzewo stanow

createBSTStates([], [], [], D) :- closeBST(D).
createBSTStates([], [], [Y | YS], D) :-
    insertBST(D, Y),
    createBSTStates([], [], YS, D).

createBSTStates([], [X | XS], Fin, D) :-
    insertBST(D, X),
    createBSTStates([], XS, Fin, D).


createBSTStates([fp(S1, _, S2) | FS], Start, Fin, D) :-
    insertBST(D, S1),
    insertBST(D, S2),
    createBSTStates(FS, Start, Fin, D).

:- op(500, xfx, --).
init(X--X).

%przeszukiwanie wszerz na listach roznic., -- z labow
wszerz(D, R) :- wszerzp([D | X] -- X, R).
wszerzp(P -- _, R) :- var(P), !, R = []. %czerwone odciecie
wszerzp([tree(L, fp(_, W, _), P) | X] -- [L, P |NK], [W|R]) :-
    wszerzp(X -- NK, R).
wszerzp([empty | P] -- K, R) :- wszerzp(P--K, R).



%przeszukiwanie wszerz na listach roznic. ale ustalone termy jako elementy
wszerzZnaki(D, R) :- wszerzZnakip([D | X] -- X, R).
wszerzZnakip(P -- _, R) :- var(P), !, R = []. %czerwone odciecie
wszerzZnakip([tree(L, W, P) | X] -- [L, P |NK], [W|R]) :-
    wszerzZnakip(X -- NK, R).
wszerzZnakip([empty | P] -- K, R) :- wszerzZnakip(P--K, R).

%przeszukiwanie wszerz na listach roznic. ale ustalone termy
%jako elementy (tutaj tranzycje)

wszerzTrans(D, R) :- wszerzTransp([D | X] -- X, R).
wszerzTransp(P -- _, R) :- var(P), !, R = []. %czerwone odciecie
wszerzTransp([tree(L, W, P) | X] -- [L, P |NK], [W|R]) :-
    wszerzTransp(X -- NK, R).
wszerzTransp([empty | P] -- K, R) :- wszerzTransp(P--K, R).

%sprawdza czy niepusta lista -- tutaj dla alfabetu
nonEmptyAlphabet(X) :- X \== [].



%sprawdza czy istnieja odpowiednie tranzycje
%dla kazdego stanu i kazdej literki, sprawdza czy istnieja tranzycja
%pomocniczo trzyma liste literek - Alphabet do iteowania sie

checkerAllTrans(Alphabet, States, TransTree) :-
    checkerAllTrans(Alphabet, States, Alphabet, TransTree).

checkerAllTrans(_, [], _, _ ).

checkerAllTrans([], [_ | XS], Alphabet, TransTree) :-
    checkerAllTrans(Alphabet, XS, Alphabet, TransTree).

checkerAllTrans([A | AX], [X | XS], Alphabet, TransTree) :-
    isMemberBST(TransTree, fpp(X, A)),
    checkerAllTrans(AX, [X | XS], Alphabet, TransTree).



%łączy dwie listy stanów w jedną
%tworzy wszystkie możliwe kombinacja - tj iloczyn kartezjanski
%nowy stan jest reprezentowny przez ns(S1, S2), gdzie S1 i S2 to stany 
% z odpowiednio States1 i States2

mergeStates(States1, States2, R) :- mergeStates(States1, States2, States1, [],  R).

mergeStates(_, [], _, Acc, Acc).

mergeStates([], [_ | SS2], States1, Acc, R) :- 
    mergeStates(States1, SS2,  States1, Acc, R).

mergeStates([S1 | SS1], [S2 | SS2], States1, Acc, R) :- 
    mergeStates(SS1, [S2 | SS2],  States1, [ ns(S1, S2) | Acc ], R).



%dla danego alfabetu oraz listy stanów, tworzy wszystkie możliwa tranzycje
%tranzycja (p1, q1) - a > (p2, q2) istnieje jezeli w autmatach Rep1 i Rep2
%istnieja traznycje odpowiednio p1 - a -> p2 oraz q1 - a > q2

createInterTrans(Alphabet, States, Rep1, Rep2, NL) :-
    createInterTrans(Alphabet, States, Alphabet, Rep1, Rep2, [], NL).

createInterTrans(_ , [] , _, _, _, Acc, Acc).

createInterTrans([] , [_ | NSS] ,Alphabet, Rep1, Rep2, Acc, NL) :-
    createInterTrans(Alphabet, NSS, Alphabet, Rep1, Rep2, Acc, NL).

createInterTrans([A | AX] , [ns(S1, S2) | NSS] ,Alphabet, Rep1, Rep2, Acc, NL) :-
    isMemberBST(Rep1, fp(S1, A, NS1)),
    isMemberBST(Rep2, fp(S2, A, NS2)),
    createInterTrans(AX, [ns(S1, S2) | NSS], Alphabet, Rep1, Rep2,
        [fp(ns(S1, S2), A, ns(NS1, NS2)) | Acc], NL).

% dla danej listy stanów, tworzy listę stanów końcowych
% ns(p1, q1) jest końcowy jeśli p1 i q1 są końcowe odpowiednio dla automatów
%Aut1 i Aut2 - reprezentuje tutaj ich stany końcowe w drzewie - Fin1Tree i Fin2Tree

createInterFin(MergedStated, Fin1Tree, Fin2Tree, NewFin) :- 
    createInterFin(MergedStated, Fin1Tree, Fin2Tree, [], NewFin).

createInterFin(_,tree(_, _, _), empty, _, []).
createInterFin(_,empty, tree(_, _, _), _, []).
createInterFin(_, empty, empty, _, []).

createInterFin([], tree(_, _, _), tree(_, _, _), Acc, Acc).
createInterFin([ns(S1, S2) | NSS], tree(L1, W1, P1), tree(L2, W2, P2), Acc, NewFin) :-
    (isMemberBST(tree(L1, W1, P1), S1) -> 
        (isMemberBST(tree(L2, W2, P2), S2) ->
        createInterFin(NSS, tree(L1, W1, P1), tree(L2, W2, P2),
            [ns(S1, S2) | Acc], NewFin)
        ; createInterFin(NSS, tree(L1, W1, P1), tree(L2, W2, P2), Acc, NewFin))
    ; createInterFin(NSS, tree(L1, W1, P1), tree(L2, W2, P2), Acc, NewFin)).
    

%analogicznia jak member, tutaj na drzewie
%dziala tez jako generator
isMemberBST(tree(_, W, _), W).
isMemberBST(tree(L, _, _), E) :- isMemberBST(L, E).
isMemberBST(tree(_, _, R), E) :- isMemberBST(R, E).


example(a11, dfa([fp(1,a,1),fp(1,b,2),fp(2,a,2),fp(2,b,1)], 1, [2,1])).
example(a12, dfa([fp(x,a,y),fp(x,b,x),fp(y,a,x),fp(y,b,x)], x, [x,y])).
example(a2, dfa([fp(1,a,2),fp(2,b,1),fp(1,b,3),fp(2,a,3),
fp(3,b,3),fp(3,a,3)], 1, [1])).
example(a3, dfa([fp(0,a,1),fp(1,a,0)], 0, [0])).
example(a4, dfa([fp(x,a,y),fp(y,a,z),fp(z,a,x)], x, [x])).
example(a5, dfa([fp(x,a,y),fp(y,a,z),fp(z,a,zz),fp(zz,a,x)], x, [x])).
example(a6, dfa([fp(1,a,1),fp(1,b,2),fp(2,a,2),fp(2,b,1)], 1, [])).
example(a7, dfa([fp(1,a,1),fp(1,b,2),fp(2,a,2),fp(2,b,1),
fp(3,b,3),fp(3,a,3)], 1, [3])).

%example(a77, dfa([fp(1,a,1), fp(3,a,3)], 1, [3])).
%example(a14, dfa([fp(1,a,2), fp(1, b, 3), ], 1, [3])).
example(b1, dfa([fp(1,a,1),fp(1,a,1)], 1, [])).
example(b2, dfa([fp(1,a,1),fp(1,a,2)], 1, [])).
example(b3, dfa([fp(1,a,2)], 1, [])).
example(b4, dfa([fp(1,a,1)], 2, [])).
example(b5, dfa([fp(1,a,1)], 1, [1,2])).
example(b6, dfa([], [], [])).
